import 'package:blood_sugar/feature_login/domain/entity/login_entity.dart';
class LoginDto extends LoginEntity {
  LoginDto({
     int? result,
     Data? data,


  }) : super(
          result: result ?? 0,
    data: data ?? Data(supportTel: '', updateConfig: UpdateConfig(downloadLink: '', forceUpdate: '', suggestUpdate: '', updateMessage: ''))

        );

  factory LoginDto.fromJson(Map<String, dynamic> json) => LoginDto(
    result: json["result"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "result": result,
    "data": data.toJson(),
  };
}



