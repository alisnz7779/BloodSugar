

import 'package:blood_sugar/core/dataSources/remote/remote_service.dart';
import 'package:blood_sugar/core/resources/data_state.dart';
import 'package:blood_sugar/core/utils/exception_parser.dart';
import 'package:blood_sugar/feature_login/domain/entity/login_entity.dart';
import 'package:blood_sugar/feature_login/domain/params/loginParams.dart';
import 'package:blood_sugar/feature_login/domain/repository/login_repository.dart';
import 'package:dio/dio.dart';


class LoginRepositoryImpl implements LoginRepository {
  final RemoteService _remoteService;

  const LoginRepositoryImpl(this._remoteService);

  @override
  Future<DataState<LoginEntity>> appVersion({required LoginParam loginParam}) async{
    try {
      final httpResponse = await _remoteService.appVersion(loginParam: loginParam);

      if (ExceptionParser.isResponseSuccessful(httpResponse)) {
        return DataSuccess(httpResponse.data);
      }
      return ExceptionParser.getApiDioError(httpResponse);
    } on DioError catch (e) {
      return DataFailed(e);
    }
  }
}
