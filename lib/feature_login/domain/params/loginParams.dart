import 'dart:convert';

LoginParam LoginFromJson(String str) => LoginParam.fromJson(json.decode(str));

String LoginToJson(LoginParam data) => json.encode(data.toJson());

class LoginParam {
  String appVersion;
  String appOs;
  String appStore;
  String appOsVersion;
  AppPosition appPosition;
  String appGuid;


  LoginParam({
    required this.appVersion,
    required this.appOs,
    required this.appStore,
    required this.appOsVersion,
    required this.appPosition,
    required this.appGuid,
  });


  factory LoginParam.fromJson(Map<String, dynamic> json) => LoginParam(
    appVersion: json["app_version"],
    appOs: json["app_os"],
    appStore: json["app_store"],
    appOsVersion: json["app_os_version"],
    appPosition: AppPosition.fromJson(json["app_position"]),
    appGuid: json["app_guid"],
  );

  Map<String, dynamic> toJson() => {
    "app_version": appVersion,
    "app_os": appOs,
    "app_store": appStore,
    "app_os_version": appOsVersion,
    "app_position": appPosition.toJson(),
    "app_guid": appGuid,
  };


}
class AppPosition {
  AppPosition({
    required this.longitude,
    required this.latitude,
  });

  String longitude;
  String latitude;

  factory AppPosition.fromJson(Map<String, dynamic> json) => AppPosition(
    longitude: json["Longitude"],
    latitude: json["Latitude"],
  );

  Map<String, dynamic> toJson() => {
    "Longitude": longitude,
    "Latitude": latitude,
  };
}


