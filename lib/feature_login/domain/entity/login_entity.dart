class LoginEntity {
  int result;
  Data data;



  LoginEntity({
    required this.result,
    required this.data,

  });
}
class Data {
  Data({
   required this.supportTel,
   required this.updateConfig,
  });

  String supportTel;
  UpdateConfig updateConfig;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    supportTel: json["support_tel"],
    updateConfig: UpdateConfig.fromJson(json["update_config"]),
  );

  Map<String, dynamic> toJson() => {
    "support_tel": supportTel,
    "update_config": updateConfig.toJson(),
  };
}
class UpdateConfig {
  UpdateConfig({
  required  this.downloadLink,
   required this.forceUpdate,
   required this.suggestUpdate,
   required this.updateMessage,
  });

  String downloadLink;
  String forceUpdate;
  String suggestUpdate;
  String updateMessage;

  factory UpdateConfig.fromJson(Map<String, dynamic> json) => UpdateConfig(
    downloadLink: json["download_link"],
    forceUpdate: json["force_update"],
    suggestUpdate: json["suggest_update"],
    updateMessage: json["update_message"],
  );
  Map<String, dynamic> toJson() => {
    "download_link": downloadLink,
    "force_update": forceUpdate,
    "suggest_update": suggestUpdate,
    "update_message": updateMessage,
  };
}
