import 'package:blood_sugar/core/dataSources/remote/remote_service.dart';
import 'package:blood_sugar/feature_login/data/repository/login_repository_impl.dart';
import 'package:blood_sugar/feature_login/domain/repository/login_repository.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';

import '../../core/datasources/remote/dio/dio_provider.dart';
import '../domain/usecase/login_usecase.dart';
import 'Login_controller.dart';

class BloodSugarBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(() => LoginController(
        Get.find<LoginUseCase>(),
    ));
    Get.lazyPut<LoginUseCase>(() => LoginUseCase(Get.find<LoginRepository>()));
    Get.lazyPut<LoginRepository>(() => LoginRepositoryImpl(Get.find<RemoteService>()));
    Get.lazyPut<Dio>(() => DioProvider().dio);
    Get.lazyPut<RemoteService>(() => RemoteService(Get.find<Dio>()));

  }
}
