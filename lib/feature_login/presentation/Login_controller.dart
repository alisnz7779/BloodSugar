import 'package:blood_sugar/core/resources/data_state.dart';
import 'package:blood_sugar/core/utils/dart_utils.dart';
import 'package:blood_sugar/feature_login/domain/entity/login_entity.dart';
import 'package:blood_sugar/feature_login/domain/params/loginParams.dart';
import 'package:blood_sugar/feature_login/domain/usecase/login_usecase.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LoginController extends GetxController {
  final LoginUseCase _loginUsecase;
  RxBool showProgress = false.obs;
  RxBool hasFailed = false.obs;
  RxString token = ''.obs;

  Rx<TextEditingController> emailController = TextEditingController().obs;
  Rx<TextEditingController> passwordController = TextEditingController().obs;
  final storage = const FlutterSecureStorage();


  LoginController(
    this._loginUsecase,
  );

  Future<DataState<LoginEntity>> appVersion({required LoginParam loginParam}) async {
    showProgress.value = true;
    var dataState = await _loginUsecase.call(params: loginParam);
    if (dataState is DataSuccess) {
      showProgress.value = false;
      final data = dataState.data;
      data?.let((it) async {
      // await  saveToStorage(it.token);
      //   print('${it.token}');

      });
    }
    if (dataState is DataFailed) {
      hasFailed.value=true;
      showProgress.value = false;
    }

    return dataState;
  }
  saveToStorage(String token)async{
     await storage.write(key: 'token', value: token);
  }

}
