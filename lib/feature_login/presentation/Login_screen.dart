import 'package:blood_sugar/core/constants/constants.dart';
import 'package:blood_sugar/feature_login/domain/params/loginParams.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:openpgp/openpgp.dart';
import '../../core/utils/toast_utils.dart';
import 'Login_controller.dart';

class LoginScreen extends StatelessWidget {
  static const Type className = LoginScreen;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final LoginController _controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return GetX<LoginController>(
      initState: (_) async {},
      builder: (logic) {
        return SafeArea(
          child: ModalProgressHUD(
            inAsyncCall: _controller.showProgress.value,
            child: Scaffold(
                // appBar: AppBar(
                //   title: const Center(
                //     child: Text(
                //       'login',
                //       style: iransans12BoldLightBlack,
                //     ),
                //   ),
                // ),
                key: _scaffoldKey,
                body: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          controller: _controller.emailController.value,
                          decoration: const InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: kLoginTextFieldContentPadding,
                                  horizontal: kLoginTextFieldContentPadding),
                              isDense: true,
                              labelText: 'email',
                              labelStyle: kLabelStyle,
                              border: kLoginTextfieldDecoration,
                              enabledBorder: kLoginTextfieldDecoration,
                              focusedBorder: kLoginTextfieldDecoration,
                              filled: true,
                              hintStyle: kLoginTextStyleHint,
                              hintText: "email",
                              fillColor: kLoginTextfieldsInsideColor),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: TextField(
                          decoration: const InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: kLoginTextFieldContentPadding,
                                  horizontal: kLoginTextFieldContentPadding),
                              isDense: true,
                              labelText: 'password',
                              labelStyle: kLabelStyle,
                              border: kLoginTextfieldDecoration,
                              enabledBorder: kLoginTextfieldDecoration,
                              focusedBorder: kLoginTextfieldDecoration,
                              filled: true,
                              hintStyle: kLoginTextStyleHint,
                              hintText: "password",
                              fillColor: kLoginTextfieldsInsideColor),
                          controller: _controller.passwordController.value,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ElevatedButton(
                          onPressed: () async {
                            // var x = await OpenPGP.encrypt("dadad",publicKey);
                            //
                            // // options.data = await OpenPGP.encryptBytes(bytesSample,publicKey);
                            // // print(x);
                            // var result = await OpenPGP.decrypt(x,privateKey,"");
                            //
                            // print(result);

                            await _controller.appVersion(
                                loginParam: LoginParam(
                              appStore: 'google',
                              appPosition: AppPosition(
                                  latitude: '9.96233', longitude: '49.80404'),
                              appVersion: '1.0.0',
                              appGuid: '88799b8e-285e-11ed-a637-50ebf6b3c36b',
                              appOsVersion: '6.1',
                              appOs: 'android',
                            ));
                            // ToastUtils.showSnackBar(toastType: ToastyType.success)
                          },
                          child: const Text('login'),
                        ),
                      ),
                      Container(
                        color: Colors.grey,
                        child: Text(_controller.token.value != ''
                            ? '${_controller.token}'
                            : ''),
                      )
                    ],
                  ),
                )),
          ),
        );
      },
    );
  }
}
