import 'package:blood_sugar/feature_encrypt/presentation/encryption_screen.dart';
import 'package:blood_sugar/feature_login/presentation/Login_binding.dart';
import 'package:blood_sugar/feature_login/presentation/Login_screen.dart';
import 'package:blood_sugar/feature_home/presentation/home_screen.dart';
import 'package:get/get.dart';

import 'feature_encrypt/presentation/encryption_binding.dart';
import 'feature_home/presentation/home_binding.dart';

class AppPages {
  static final pages = [

    GetPage(
      name: '/${HomeScreen.className}',
      page: () =>   HomeScreen(),
      binding:HomeBinding(),
    ),

    GetPage(
      name: '/${LoginScreen.className}',
      page: () =>   LoginScreen(),
      binding:BloodSugarBinding(),
    ),
    GetPage(
      name: '/${EncryptionScreen.className}',
      page: () =>   EncryptionScreen(),
      binding:EncryptionBinding(),
    ),


  ];
}
