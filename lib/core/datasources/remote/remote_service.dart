import 'dart:io';

import 'package:blood_sugar/core/constants/constants.dart';
import 'package:blood_sugar/feature_login/data/dto/login_dto.dart';
import 'package:blood_sugar/feature_login/domain/params/loginParams.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

 part 'remote_service.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class RemoteService {
  factory RemoteService(Dio dio, {String baseUrl}) = _RemoteService;


  @POST('proxy/app_get_config')
  Future<HttpResponse<LoginDto>> appVersion({
    @Body() required LoginParam loginParam,
  });


}
