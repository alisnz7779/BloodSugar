import 'package:flutter/material.dart';

const String kBaseUrl = 'https://golrang-staging.esavis.ir:4040/';
// const String kBaseUrl = 'http://192.168.15.186:8000/';
const int kConnectTimeout = 50000;
const int kReceiveTimeout = 50000;
const String kUnknownInputCast = "Unknown Input Cast";
const String faPrimaryFontFamily = 'iransans';

const String publicKey = '''
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: openpgp-mobile

xsBNBGMN6FwBCADhchKbhYX0VIovQp/ghkR3cZVBVfM17XEg98iI1LoEwuJzFHUh
VPz1gDVtSjjq6P+q5Vtt+F9Fsg0iJO0kOmOoMOFlXJdCScDIMvzlEkV14pGNpP2y
wUFcNkCvpGN7hxLNDlQfjMEdwc0mqfhLlmHzjUranmLe4mKnOEJ30k3J+n2Eekpn
eTgpvvJTMzgygPbgMCmCeN7bk4TKbb3h1vS5u5efVogqps4QJkTERqVFtimchgFU
RWe0VYqqxASqgLEL0Jbn85l7uLyXKvB7VeHbyWw4Xllet6nWynkhUP57QxPbWLVo
82IzYgyzDcGbVHptvxWkvilqI79oCTEtNyfTABEBAAHNIFNBVklTIChmaW50ZWNo
KSA8b21heEBlc2F2aXMuaXI+wsCJBBMBCAA9BQJjDehcCZA8/NKpR36QaxYhBNLz
ccyf/DAddou34jz80qlHfpBrAhsDAh4BAhkBAwsJBwIVCAIWAAIiAQAAylUIALAK
bEPtOS3aeVIC66XoMmHFaAgs/5QUymKbhot1RyrhC0kI1a7uQl08a73oGQN83GME
aJOO8a4+DHrmBdQdafMQjHLVkQjRABnsm3ELCMTTCz9Kr/k7nIYdv6rFe4N8cq3G
wo1XrNSSgo4OH04mpdBlUOJE3SY9XamBc0zcpAAI3kwxpAemktK0W7PAXxyHe9yA
qDJz8C5R0jIEE/ta06e36qgw36owCdztKg5AD1UGdXE+Srs0bbr7EdRAAfNhX68S
MYIonb5FKpZ5xYYxor7YOg19wFqRkRYlN1dczv/KDqmCOghdmr3od9CEq6mC9M09
lXjcoEQRHvpx/JQRL6HOwE0EYw3oXAEIANNxXZ6wdU8ZeWE5cbgPf/45mzXKp3dX
14xgthsKgwHFoDxvybU08hAHdiq1gtTonSeJJj1KhwstYmTHj+0xXGAip3/V5quo
1PQw1J7+HEojtrwg/jl8NJ9kRlAlzF037+GgO7iP/zsl25j6wP3RDKnqXYpYuIeA
TK02JeZR6g/In8hebxWFUJ3srYI8cuuDaI6WOoYwsjGthuS9r+Mu6zzc0AsYOLD8
P6kiWfZsYydSQf/W7Vs/IDHg6G1tHNIOTvSxiKMpwOpUrT09pqO55lEqN32d65Rh
roNcwrgcozQ4suyYyaoJP+W687FIgAaQp7DCUPTnIYCT2UdGLRgSrVMAEQEAAcLA
dgQYAQgAKgUCYw3oXAmQPPzSqUd+kGsWIQTS83HMn/wwHXaLt+I8/NKpR36QawIb
DAAA0TYIANd4zvKYSf6cMwxesohMfaEQtyWNsxwGC+a4zpXfcRay+VNTm38iVGep
+Z4glIU02uJD4r8jjRBTagvfJLKFKCwv9ThbtIIIZI+nG9PpQsso/VFeqyGdMhbJ
26fYPBQS4wVhCO1UschVsn9xrEHe9Xuh1k1OQiWd32FvXeTZr0mecwlxQmOegpnk
ucqp7y6MdKWAmSM84fg4B2Ff0/IZGnlA44RPN4gSBgU8CrlosAQddCPmnYiGkp0X
b4QuPzyn9vLHKTueYb72IJZKTgfa6wPkuwLBBCX46HyRFz52sezFyavRUE4vty2E
ppWZyucbNndrPhTY5kIVQPwcNduNBFM=
=0gzb
-----END PGP PUBLIC KEY BLOCK-----
''';

const String privateKey = '''
-----BEGIN PGP PRIVATE KEY BLOCK-----
Version: openpgp-mobile

xcLYBGMN6FwBCADhchKbhYX0VIovQp/ghkR3cZVBVfM17XEg98iI1LoEwuJzFHUh
VPz1gDVtSjjq6P+q5Vtt+F9Fsg0iJO0kOmOoMOFlXJdCScDIMvzlEkV14pGNpP2y
wUFcNkCvpGN7hxLNDlQfjMEdwc0mqfhLlmHzjUranmLe4mKnOEJ30k3J+n2Eekpn
eTgpvvJTMzgygPbgMCmCeN7bk4TKbb3h1vS5u5efVogqps4QJkTERqVFtimchgFU
RWe0VYqqxASqgLEL0Jbn85l7uLyXKvB7VeHbyWw4Xllet6nWynkhUP57QxPbWLVo
82IzYgyzDcGbVHptvxWkvilqI79oCTEtNyfTABEBAAEAB/9Da4jTB7BSrEGS9pq3
W4/4JHidTGXqd7LASDmpaeyjZeCwx6prqG3pwUi5AVxaLAnhgqvADGn5ftDcqCxf
DDMnZk7RsEnv4fyMImts2fT51yLuOCxCm/a+696WaFPz6iYeOq2/ralHC+EdMfF2
MWfjnRqInJs2yn70QSfni3YF85piC6Ok5XLdm8MDLshqfbI939GTFLTtBx2B+1XP
t5jZpQS5ZvBVCHo53hd6RIo2jzeGTGT0uzLVwrr5ShSERdHobqGS6ltx5rsBl6u0
epVRkqcT/HgC3q76lTztxAZ2d+JHouE0z6MaQ3178ucAWy4Uog/DZjpOFvkiBA6I
to3xBAD75POm71gYugLgCIychavoM+WmMLjWx829vI2jHlX536Bi1Lds+YobmU9+
mgXRfG8p81iqzyftXFLQZHmYjoMl9AQ/9Yy8wOXdHXKmv9TseBtpVTiadPVVC2D4
7LMLzsPnny3MxbtUh4I+0Dbvfudjm+VWRc9I6UPZY0/FXfXXGwQA5R7C8wkJayfO
xJmm3IzKO4Ukz3wKdUcyWqOlJVfXprRBL3TtRaSzAo4jIR+U2SCT0IbdVJ/Wd1Bg
CD2hRfohLH7WH3dErK9NBBIXTzQTBO5VAxnbR6qslczVQTU+mzEfaAMz3wqHCi9m
VPrAG1UmzCJgEiNU8kog6u4L36iT5akD/35Gbi9AbBnEmQeSA9xzyZbqLOxBbWGt
hbkxKmEPk9fO2fF5saFUMlTMf28n0nKnFMRDEoCoF/eFx3im/gvU6zdwKu6Z5iMl
8HzSO3qMqyY+JRxD0n4WHup2lzFE+SdKu/uppuBQMcUh+2T1E9yaBuDe0IfikPXU
QINo2FzcR17QSnHNIFNBVklTIChmaW50ZWNoKSA8b21heEBlc2F2aXMuaXI+wsCJ
BBMBCAA9BQJjDehcCZA8/NKpR36QaxYhBNLzccyf/DAddou34jz80qlHfpBrAhsD
Ah4BAhkBAwsJBwIVCAIWAAIiAQAAylUIALAKbEPtOS3aeVIC66XoMmHFaAgs/5QU
ymKbhot1RyrhC0kI1a7uQl08a73oGQN83GMEaJOO8a4+DHrmBdQdafMQjHLVkQjR
ABnsm3ELCMTTCz9Kr/k7nIYdv6rFe4N8cq3Gwo1XrNSSgo4OH04mpdBlUOJE3SY9
XamBc0zcpAAI3kwxpAemktK0W7PAXxyHe9yAqDJz8C5R0jIEE/ta06e36qgw36ow
CdztKg5AD1UGdXE+Srs0bbr7EdRAAfNhX68SMYIonb5FKpZ5xYYxor7YOg19wFqR
kRYlN1dczv/KDqmCOghdmr3od9CEq6mC9M09lXjcoEQRHvpx/JQRL6HHwtgEYw3o
XAEIANNxXZ6wdU8ZeWE5cbgPf/45mzXKp3dX14xgthsKgwHFoDxvybU08hAHdiq1
gtTonSeJJj1KhwstYmTHj+0xXGAip3/V5quo1PQw1J7+HEojtrwg/jl8NJ9kRlAl
zF037+GgO7iP/zsl25j6wP3RDKnqXYpYuIeATK02JeZR6g/In8hebxWFUJ3srYI8
cuuDaI6WOoYwsjGthuS9r+Mu6zzc0AsYOLD8P6kiWfZsYydSQf/W7Vs/IDHg6G1t
HNIOTvSxiKMpwOpUrT09pqO55lEqN32d65RhroNcwrgcozQ4suyYyaoJP+W687FI
gAaQp7DCUPTnIYCT2UdGLRgSrVMAEQEAAQAIAIqYjrNMGzjkC8oM61uo3U7416Ik
2izCKYncgsGq3EC3F9dFt0POFrUnDu/j1oRUjV6y6ZvXVDmhYBOLM+5YIqLpVD3Z
KcBvhFckAtrM2iJG0qryPXHU1nF8OxKiFBkuZ1DPxRWvXa+Zmx3lj8dP1isycmoS
ybIYCSqoiaM1ImWA/NiXZ2zWJUzfDLdFeAdReRjU67dhPPQHyQALK3cqTpjto0YH
H8gYGpf0UeaPElJFB/mAgXPeXL7ZOJG5v7/Ip+5+9uhOPRTCbqwgXPvosh3tcZd9
nwiZjDKfmgllhTkZOewn7ORh2KFcHi6ACpOp4mHadEdU6sVn4OhhQz/l/mEEAPN7
rdoGIRb4O1WTp+K5/DanRzubSuryVFJ6884CQH18+0om94llwTac4cfkznZ+NQz5
4i38gwBTLnB+bVjRr55Q2t+/enBENtPhWMd58yqutnag1VXAot1+jMhjBCVI03Ht
2QAYudvA/8eRxmPbWJNHiun1NNm4xTnpja88tN7JBADeUAaG/qX9xiIVixgyV3G3
EYYzFWTH94jNMvM1BVsjzI+a6uUj9T0LpVFOIKDpKROEDW0YlzHIC9tjecD1KBeb
6yd4tO7kgVzLb0eKExXNCDQ64TK3qatQGBYFB4UgWsvRxjVJarOJjICJFamBrnPE
lOezphK8tk6E8/EhkGotOwQAv9xk8L4gy8E/hZYib9v0d8frtQMK6sOBUwQaNzfr
UcIUseeDap1BnAtlRmAXwsDkKo1sdrGZGY0s5Y/UBJ9xkFe0AVcVMyeTuIgYuOHC
nQyWxeFQisU09koV50Z52DNz0YGP4nOc7G+7CjuHw2D+Fa3bUNkwZhPyPgSNdPgQ
JldDIcLAdgQYAQgAKgUCYw3oXAmQPPzSqUd+kGsWIQTS83HMn/wwHXaLt+I8/NKp
R36QawIbDAAA0TYIANd4zvKYSf6cMwxesohMfaEQtyWNsxwGC+a4zpXfcRay+VNT
m38iVGep+Z4glIU02uJD4r8jjRBTagvfJLKFKCwv9ThbtIIIZI+nG9PpQsso/VFe
qyGdMhbJ26fYPBQS4wVhCO1UschVsn9xrEHe9Xuh1k1OQiWd32FvXeTZr0mecwlx
QmOegpnkucqp7y6MdKWAmSM84fg4B2Ff0/IZGnlA44RPN4gSBgU8CrlosAQddCPm
nYiGkp0Xb4QuPzyn9vLHKTueYb72IJZKTgfa6wPkuwLBBCX46HyRFz52sezFyavR
UE4vty2EppWZyucbNndrPhTY5kIVQPwcNduNBFM=
=HXW1
-----END PGP PRIVATE KEY BLOCK-----
''';

// region <<< Crane >>>
const kColorOffWhite = Color(0xffF6F6F8);
const kColorDarkGray = Color(0xff303030);
const kColorGray = Color(0xff707070);
const kColorGray2 = Color(0xff505050);
const kColorGray3 = Color(0xffE5E5E5);
const kColorGray4 = Color(0xff909090);
const kColorGray5 = Color(0xffD2D1D1);
const kColorYellow = Color(0xffFBCC34);
const kColorGreen = Color(0xff43CF53);
const kColorGreen2 = Color(0xff85CFAD);
const kColorPurple = Color(0xff5F5CF6);
const kColorOffPurple = Color(0xffBDBDED);
const kColorOffPurple2 = Color(0xffF2F1FE);
const kColorRed = Color(0xffF24A37);
const kBGToastDarkGreen=Color(0xff3eBE61);
const textLightBlackColor=Color(0xff505050);
const kBGToastLightGreen=Color(0xffEBF7EE);
const kBGToastDarkBlue=Color(0xff006ce4);
const kBGToastLightBlue=Color(0xffE5EFFA);

const kLoginTextfieldDecoration = OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(30.0),
    ),
    borderSide: BorderSide(color: kColorDarkBlue, width: 2.0));
const iransans12BoldLightBlack = TextStyle(
    color: textLightBlackColor,
    fontFamily: 'iransans',
    fontWeight: FontWeight.bold,
    decoration: TextDecoration.none,
    fontSize: 16);
// endregion

const titleTextStyle =TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 20,
    fontFamily: 'iransans',
    color: kColorRed
);
const headlineTextStyle =TextStyle(
    fontWeight: FontWeight.bold,
    fontSize: 14,
    fontFamily: 'iransans',
    color: kColorDarkBlue
);





const kPurple=Color(0xff5855C6);
const iransansBoldSize16Purple = TextStyle(
    color: Colors.white,
    fontFamily: 'iransans',
    fontWeight: FontWeight.bold,
    decoration: TextDecoration.none,
    fontSize: 16);
const veryLightPurple=Color(0xffDAD9F2);

const kMapApiKey='eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI5MjU1Zjg4ZjAzNTRkNWRlOWQzNmMxZjIxZmNjNWY3ZDNiMDFjNTdlNTE0YzVjYzg3MmQ4YTQ3NWM0YTQxYzlmNGI1NGZlOGZkMTMyZmFkIn0.eyJhdWQiOiIxMDczOCIsImp0aSI6IjI5MjU1Zjg4ZjAzNTRkNWRlOWQzNmMxZjIxZmNjNWY3ZDNiMDFjNTdlNTE0YzVjYzg3MmQ4YTQ3NWM0YTQxYzlmNGI1NGZlOGZkMTMyZmFkIiwiaWF0IjoxNTk5Mjg2MTM2LCJuYmYiOjE1OTkyODYxMzYsImV4cCI6MTYwMTk2ODEzNiwic3ViIjoiIiwic2NvcGVzIjpbImJhc2ljIl19.mw4TE_PkVWS9uIefxhqXTXtQpI1XY1cS1Uponmm68RCUXnD6tnMTCwvbu9mLnqO3SubrsuZvlhb4JR6qHLlAPlBgxFm51qxUSiMhZcA8aCAiHJVMqO_tDRDiyEhkYclc8dRPbEGM7FKE160_ke2bXL2w-iM71r2ns6LE2jqnQLD9mAO60HeVIRPjNL5LCF_prBhEWu4qSauvxpiZZMfv96hcYIDv0uxYs3pUXzzhqoIRmBAm1XUN4v2jr_-4Gnq71Z4DEudl36hspDE0iJ3xji-8e5UGdeE4u0TdyMNS0guvmUOGGDBeT7NHrgnxArZhnRNCTX9RlrTLaGk-dcH_tg';
const kSlideTitleStyle = TextStyle(
    fontSize: 22.0,
    color: Color(0xffef353c), //kColorRedCoTop,
    fontFamily: 'iransans',
    fontWeight: FontWeight.w700);
const kSlideButtonsStyle = TextStyle(
    fontSize: 20.0,
    color: kSliderButtonsColor,
    fontFamily: 'iransans',
    fontWeight: FontWeight.w300);
const kSlideButtonsStyle2 = TextStyle(
    fontSize: 20.0,
    color: kColorPallete2,
    fontFamily: 'iransans',
    fontWeight: FontWeight.w300);
const kSideBarStyle=TextStyle(
    fontSize: 14,
    fontFamily: 'iransans',
    fontWeight: FontWeight.bold,
    color: kColorDarkBlue);
const kTextStyleForgetPass = TextStyle(
    fontFamily: 'iransans',
    fontSize: 15.0,
    color: kColorPallete6,
    fontWeight: FontWeight.w300);

const kSlideDescriptionStyle = TextStyle(
    fontSize: 18.0,
    color: Colors.black45, // kColorGreyIntroText,
    fontFamily: 'iransans',
    fontWeight: FontWeight.w300);

const kLoginTitleStyle = TextStyle(
  fontFamily: 'iransans',
  fontSize: 18.0,
  color: Colors.grey,
  fontWeight: FontWeight.w700,
);
const kLoginTitleStyle1 = TextStyle(
  fontFamily: 'iransans',
  fontSize: 25.0,
  color: Colors.black45,
  fontWeight: FontWeight.bold,
);
const kDialog = TextStyle(
    color:
    kColorDarkBlue,
    fontFamily:
    'iransans',
    fontSize: 12,
    fontWeight:
    FontWeight
        .bold);
const kLoginTitleOpacity = 0.85;

const kLoginDescriptionStyle = TextStyle(
  fontFamily: 'iransans',
  fontSize: 18.0,
  color: kLoginDescriptionColor,
  fontWeight: FontWeight.w500,
);

const kSliderButtonsColor = Color(0xff4983c4);
const kSliderDotsColor = Color(0xffc2c2c2);
const kSliderActiveDotsColor = Color(0xff4983c4); //Color(0xff8855aa);
const kColorDarkBlue = Color(0xff1C4870);
const kColorGrey = Color(0xff707070);
const kColorGreyHigh=Color(0xff4F4F4F);
const kColorGreyLight = Color(0xffcfcfd0);
const kColorDarkRed = Color(0xffB00020);
const kColorGreyPelak = Color(0xff969798);
const kColorDarkBluePelak = Color(0xff00319D);
const kColorGreySpec = Color(0xff505050);

const kColorPallete1 = Color(0xff000000);
const kColorPallete2 = Color(0xff14213d);
const kColorPallete3 = Color(0xfffca311);
const kColorPallete4 = Color(0xffe5e5e5);
const kColorPallete5 = Color(0xffd62828);
const kColorPallete6 = Color(0xff283618);
const kSplashBackground = Color(0xff79c6df);
const kColorGreyButton = Color(0xffCFCFD0);


const kColorGoogle = Color(0xffdb3236);
const kColorFacebook = Color(0xff3b5998);

const kColorBlueCoTop = Color(0xff246dc5);
const kColorBlueCoBottom = Color(0xff186491);
const kColorRedCoTop = Color(0xffc72d31);
const kColorRedCoBottom = Color(0xffd5343a);

const kColorGreyIntroTop = Color(0xffececec);
const kColorGreyIntroBottom = Color(0xffededed);
const kColorGreyIntroBack = LinearGradient(
    colors: [kColorGreyIntroTop, kColorGreyIntroBottom],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter);
const kColorGreyIntroText = Color(0xffc2c2c2);
const kColorIntroButtonBack = Color(0xff186491);
const kColorIntroButtonText = Color(0xffffffff);
const kLoginTitleColor = Color(0xff185191);
const kLoginDescriptionColor = Color(0xff9a9a9a);
const kColorIntroButtonStyle = TextStyle(
    fontFamily: 'iransans',
    fontWeight: FontWeight.w500,
    fontSize: 18.0,
    color: kColorIntroButtonText,
    decoration: TextDecoration.none);
const kColorSpecBlue = Color(0xff29689F);
const kLoginBorderRadius = 5.0;
const kLoginTextPadding = 16.0;
const kLoginBorderPadding = 40.0;
const kSplashPadding = 80.0;
const kSearch = TextStyle(
  fontSize: 16,
  fontFamily: 'iransans',
  fontWeight: FontWeight.bold,
  color: kColorGreyPelak,
);
const kBazdidBorderWhite =OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(10),
    ),
    borderSide: BorderSide(color: Colors.white, width: 1.5));
const kBazdidBorderBlue = OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(10),
    ),
    borderSide: BorderSide(color: kColorDarkBlue, width: 1.5));
const kBazdidStyle = TextStyle(fontSize: 12, fontFamily: 'iransans', color: kColorSpecBlue,fontWeight: FontWeight.bold,);
const kPhoneNumber = TextStyle(
  fontSize: 14,
  fontFamily: 'iransans',
  color: kColorSpecBlue,
  fontWeight: FontWeight.w400,
);
const kPlateNumber = TextStyle(
    fontWeight: FontWeight.w400,
    fontFamily: 'iransans',
    fontSize: 20,
    color: kColorSpecBlue);
const kColorLightBlue =Color(0xffC4DBF0);
const kLoginTextfieldBoarderColor = Color(0xffe6e6e6);
const kLoginTextfieldBoarderColorRed = Colors.red;
const kLoginTextFieldContentPadding = 15.0;
const kBlueDialog = Color(0xff1C4870);
const kHomehadese = TextStyle(
    color: Color(0xff969798),
    fontSize: 20,
    fontWeight: FontWeight.bold,
    fontFamily: 'iransans');
const kHadeseTypeTitle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: kColorRed,
    fontFamily: 'iransans');

const kCamera = TextStyle(
  fontSize: 14,
  fontFamily: 'iransans',
  fontWeight: FontWeight.bold,
  color: kColorDarkBlue,
);
const kCustomButtonTextStyle  = TextStyle(
    color: Colors.white,
    fontSize: 20,
    fontFamily: 'iransans',
    fontWeight: FontWeight.bold) ;

const kCameraButton = TextStyle(
    fontWeight: FontWeight.bold, fontSize: 14, fontFamily: 'iransans');

const kHadeseType = TextStyle(
    fontSize: 10,
    fontFamily: 'iransans',
    fontWeight: FontWeight.bold,
    color: kColorRed);
const kPelakIran = TextStyle(
    fontWeight: FontWeight.bold,
    fontFamily: 'iransans',
    fontSize: 10,
    color: Colors.white);
const kHomefirst = TextStyle(
    fontSize: 20,
    color: Color(0xff969798),
    fontWeight: FontWeight.bold,
    fontFamily: 'iransans');

const kHomepageTextStyle = TextStyle(
    color: kColorDarkBlue,
    fontFamily: 'iransans',
    fontWeight: FontWeight.bold,
    fontSize: 12);


const kLoginTextfieldDecorationError = OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(50.0),
    ),
    borderSide: BorderSide(color: kColorDarkRed, width: 3.0));

const kLoginTextFieldHintColor = Color(0xffc2c2c2);
const kLoginTextFieldTextColor = Color(0xff000000);
const kUltralightBlue = Color(0xffC4DBF0);
const kLoginTextfieldsInsideColor = Colors.white;

const kSidebarText = TextStyle(
  color: kColorDarkBlue,
  fontSize: 14,
  fontFamily: 'iransans',
  fontWeight: FontWeight.bold,
);

const kLoginTextStyleHint = TextStyle(
    fontFamily: 'iransans',
    fontWeight: FontWeight.w300,
    fontSize: 17.0,
    color: kLoginTextFieldHintColor);

const kLoginTextFieldTextStyle = TextStyle(
    fontFamily: 'iransans',
    fontWeight: FontWeight.bold,
    fontSize: 16.0,
    color: kColorDarkBlue);
const kHagheBime =TextStyle(
    color: kColorDarkBlue,
    fontSize: 16,
    fontWeight: FontWeight.bold,
    fontFamily: 'iransans'
);
const kLoginErrorTextFieldTextStyle = TextStyle(
    fontFamily: 'iransans',
    fontWeight: FontWeight.bold,
    fontSize: 16.0,
    color: kColorDarkRed);

const kLabelStyle =
TextStyle(fontSize: 12, fontFamily: 'iransans', color: kColorDarkBlue);
const kLabelErrorStyle =
TextStyle(fontSize: 12, fontFamily: 'iransans', color: kColorDarkRed);

const kLoginBottomTextRightColor = Color(0xffed5c61);
const kLoginBottomTextRightStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
    color: kLoginBottomTextRightColor);

const kLoginBottomTextSignInColor = Color(0xff253d4b);
const kLoginBottomTextSignInStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 13.0,
    fontWeight: FontWeight.w700,
    color: kColorDarkBlue);

const kLoginPadding = 20.0;

const kLoginBackColorTop = Color(0xfff3f3f3);
const kLoginBackColorBottom = Color(0xffdbdbdb);

const kLoginTextChangeNumberStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
    color: kLoginBottomTextRightColor);

const kLoginTextResendColor = Color(0xff9a9a9a);
const kLoginTextResendStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 14.0,
    fontWeight: FontWeight.w300,
    color: kLoginTextResendColor);

const kInfoDividerThikness = 3.0;

const kInfoStuffSignInColor = Color(0xffed1c24);

const kMaxDigitsOfPhoneNum = 10;
const kMaxLengthOfOTPCode = 5;

const kCountDownSecondsOTP = 60;

const kTempTextStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 12.0,
    color: Colors.black45,
    decoration: TextDecoration.none);
const kMSColorOfDivider = Color(0xffc2c2c2);

const kMinCharName = 2;

const kPaddingTakePicScreenVertical = 25.0;
const kPaddingTakePicScreenHorizontal = 20.0;

const kTakePhotoTopColor = Color(0xffcce0e5);
const kTakePhotoBottomColor = Color(0xffededed);
const kTakePhotoArrowColor = Color(0xcc174561);

const kTakePhotoProfileNameStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 16.0,
    color: kTakePhotoArrowColor,
    fontWeight: FontWeight.w500,
    decoration: TextDecoration.none);

const kTakePhotoProfileNumberStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 16.0,
    color: Colors.black,
    fontWeight: FontWeight.w400,
    decoration: TextDecoration.none);
const kDialogShasi =TextStyle(
    color:
    kColorDarkBlue,
    fontFamily:
    'iransans',
    fontWeight:
    FontWeight
        .w500);
List<String> farsiCollection = [
  ' ',
  'أ',
  'إ',
  'ؤ',
  'ي',
  'ۀ',
  'ئ',
  'آ',
  'ا',
  'ب',
  'پ',
  'ت',
  'ث',
  'ج',
  'چ',
  'ح',
  'خ',
  'د',
  'ذ',
  'ر',
  'ز',
  'ژ',
  'س',
  'ش',
  'ص',
  'ض',
  'ط',
  'ظ',
  'ع',
  'غ',
  'ف',
  'ق',
  'ک',
  'گ',
  'ل',
  'م',
  'ن',
  'و',
  'ه',
  'ی',
];



Map<int, String> orders = {
  1: 'اول',
  2: 'دوم',
  3: 'سوم',
  4: 'چهارم',
  5: 'پنجم',
  6: 'ششم',
  7: 'هفتم',
  8: 'هشتم',
  9: 'نهم',
  10: 'دهم'
};

const universalTextColor = Color(0xff6e6e6e);

const universalTextStyleMain = TextStyle(
    fontFamily: 'iransans',
    fontWeight: FontWeight.w300,
    fontSize: 18.0,
    decoration: TextDecoration.none,
    color: universalTextColor);

const universalTextStyleAccent = TextStyle(
    fontFamily: 'iransans',
    fontWeight: FontWeight.w500,
    fontSize: 16.0,
    decoration: TextDecoration.none,
    color: universalTextColor);



const kBackColor = Color(0xffededed);
const kPaddingScreenHorizontal = 40.0;
const kPaddingScreenVertical = 35.0;


const kKarshenasTitleStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 20.0,
    color: kLoginTitleColor,
    fontWeight: FontWeight.w700,
    decoration: TextDecoration.none
);

const kKarshenasDescriptionStyle = TextStyle(
    fontFamily: 'iransans',
    fontSize: 16.0,
    color: kLoginDescriptionColor,
    fontWeight: FontWeight.w300,
    decoration: TextDecoration.none

);

const kTextpaddinVer=15.0;
const kTextpaddinHor = 25.0;

const kMaxNumOfDigitSerial=10;
const kIconSize = 30.0;

const kColorConfirmButton  = kColorRedCoBottom;

const kTopTextKarshenasInfo=TextStyle(fontFamily: 'iransans',color: Colors.black54,fontSize: 18.0,fontWeight: FontWeight.w700);
const kFieldsTextStyle = TextStyle(fontFamily: 'iransans',color: Colors.blueAccent,fontSize: 15.0,fontWeight: FontWeight.w300);