import 'package:blood_sugar/core/dataSources/remote/remote_service.dart';
import 'package:dio/dio.dart';

import 'package:get/get.dart';

import '../../core/datasources/remote/dio/dio_provider.dart';
import 'home_controller.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController(
    ));
    Get.lazyPut<Dio>(() => DioProvider().dio);
    Get.lazyPut<RemoteService>(() => RemoteService(Get.find<Dio>()));


  }
}
