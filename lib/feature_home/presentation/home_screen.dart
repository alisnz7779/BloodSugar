import 'package:blood_sugar/core/constants/constants.dart';
import 'package:blood_sugar/feature_encrypt/presentation/encryption_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../feature_login/presentation/Login_screen.dart';
import 'home_controller.dart';

class HomeScreen extends StatelessWidget {
  static const Type className = HomeScreen;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final HomeController _controller = Get.find();

  @override
  Widget build(BuildContext context) {

        return SafeArea(
          child: Scaffold(

            key: _scaffoldKey,
            body: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [

                  ElevatedButton(onPressed: () {
                    Get.toNamed(LoginScreen.className.toString());
                  }, child: Text('login'),),
                  ElevatedButton(onPressed: () {
                    Get.toNamed(EncryptionScreen.className.toString());

                  }, child: Text('encrypt'),)

                ],
              ),
            )

          ),
        );
  }
}
