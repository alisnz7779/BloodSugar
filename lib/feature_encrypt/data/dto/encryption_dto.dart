import 'package:blood_sugar/feature_encrypt/domain/entity/encryption_entity.dart';
import 'package:blood_sugar/feature_login/domain/entity/login_entity.dart';
class EncryptionDto extends EncryptionEntity {
  EncryptionDto({
    String? entry,


  }) : super(
    entry: entry ?? '',

  );

  factory EncryptionDto.fromJson(Map<String, dynamic> json) =>
      EncryptionDto(
        entry: json['token'],

      );

  Map<String, dynamic> toJson() => {


    "token":entry,

  };
}
