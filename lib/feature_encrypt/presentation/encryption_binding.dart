import 'package:blood_sugar/feature_encrypt/presentation/encryption_controller.dart';

import '../../core/datasources/remote/dio/dio_provider.dart';
import 'package:get/get.dart';

class EncryptionBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<EncryptionController>(() => EncryptionController(
    ));

  }
}
