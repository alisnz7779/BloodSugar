import 'package:blood_sugar/core/constants/constants.dart';
import 'package:blood_sugar/feature_encrypt/presentation/encryption_controller.dart';
import 'package:blood_sugar/feature_login/domain/params/loginParams.dart';
import 'package:encrypt/encrypt.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import '../../core/utils/generateUtils.dart';
import '../../core/utils/toast_utils.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

class EncryptionScreen extends StatelessWidget {

  static const Type className = EncryptionScreen;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final EncryptionController _controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return GetX<EncryptionController>(
      initState: (_) async {},
      builder: (logic) {
        return SafeArea(
            child: ModalProgressHUD(
          inAsyncCall: _controller.showProgress.value,
          child: Scaffold(
            // appBar: AppBar(
            //   title: const Center(
            //     child: Text(
            //       'login',
            //       style: iransans12BoldLightBlack,
            //     ),
            //   ),
            // ),
            key: _scaffoldKey,
            body: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      controller: _controller.plainTextController.value,
                      decoration: const InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: kLoginTextFieldContentPadding,
                              horizontal: kLoginTextFieldContentPadding),
                          isDense: true,
                          labelText: 'plain text',
                          labelStyle: kLabelStyle,
                          border: kLoginTextfieldDecoration,
                          enabledBorder: kLoginTextfieldDecoration,
                          focusedBorder: kLoginTextfieldDecoration,
                          filled: true,
                          hintStyle: kLoginTextStyleHint,
                          hintText: "plain text",
                          fillColor: kLoginTextfieldsInsideColor),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ElevatedButton(
                      onPressed: () async {
                         _controller.aesEncryption(_controller.plainTextController.value.text);
                         _controller.rsaEncryption(_controller.encryptionKey.value);
                      },
                      child: const Text('encrypt'),
                    ),

                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.grey,
                      child: Text(_controller.encryptionKey.value!= ''
                          ? 'key is : '+_controller.encryptionKey.value
                          : ''),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.grey,
                      child: Text(_controller.encryptionIV.value!= ''
                          ? 'iv is : '+_controller.encryptionIV.value
                          : ''),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      color: Colors.grey,
                      child: Text(_controller.encryptionKey!= ''
                          ? 'aes encrypted is : '+ _controller.aesEncryption(_controller.plainTextController.value.text)

                          : ''),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ));
      },
    );
  }
}
