import 'package:blood_sugar/core/resources/data_state.dart';
import 'package:blood_sugar/core/utils/dart_utils.dart';
import 'package:blood_sugar/core/utils/generateUtils.dart';
import 'package:blood_sugar/feature_login/domain/entity/login_entity.dart';
import 'package:blood_sugar/feature_login/domain/params/loginParams.dart';
import 'package:blood_sugar/feature_login/domain/usecase/login_usecase.dart';
import 'package:encrypt/encrypt.dart';
import 'package:encrypt/encrypt_io.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:pointycastle/asymmetric/api.dart';
// import 'package:rsa_encrypt/rsa_encrypt.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

class EncryptionController extends GetxController {
  // final LoginUseCase _loginUsecase;
  RxBool showProgress = false.obs;
  RxString encryptionKey = ''.obs;
  RxString encryptionIV = ''.obs;

  Rx<TextEditingController> plainTextController = TextEditingController().obs;

  EncryptionController(// this._loginUsecase,
      );

  String aesEncryption(String plainText) {
    encryptionKey.value = GenerateUtils.CreateCryptoRandomString();
    encryptionIV.value = GenerateUtils.CreateCryptoRandomString();
    final key = encrypt.Key.fromBase64(encryptionKey.value);
    final iv = encrypt.IV.fromBase64(encryptionIV.value);

    final encrypter = Encrypter(AES(key, mode: AESMode.cbc, padding: 'PKCS7'));
    final encrypted = encrypter.encrypt(plainText, iv: iv);


    return encrypted.base64;
  }
  rsaEncryption(String plainText)async{
    final publicPem = await rootBundle.loadString('assets/public.pem');
    final publicKey = await parseKeyFromFile<RSAPublicKey>(publicPem);
    print(publicKey);
    final privatePem = await rootBundle.loadString('assets/private.pem');
    final privateKey = await parseKeyFromFile<RSAPrivateKey>(privatePem);
    print(privateKey);
    final encrypter = Encrypter(RSA(publicKey: publicKey, privateKey: privateKey));
    final encrypted = encrypter.encrypt(plainText);
    print(encrypted.base64);
    return(encrypted.base64);

  }
}
