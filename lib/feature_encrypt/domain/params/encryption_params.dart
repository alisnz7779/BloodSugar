import 'dart:convert';

EncryptionParam LoginFromJson(String str) => EncryptionParam.fromJson(json.decode(str));

String EncryptionToJson(EncryptionParam data) => json.encode(data.toJson());

class EncryptionParam {
  final String key;
  final String iv;


  EncryptionParam({
    required this.key,
    required this.iv,

  });

  factory EncryptionParam.fromJson(Map<String, dynamic> json) => EncryptionParam(
    key: json["key"],
    iv: json["iv"],

  );

  Map<String, dynamic> toJson() => {
    "key": key,
    "iv": iv,

  };
}

